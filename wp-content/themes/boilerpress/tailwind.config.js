module.exports = {
  theme: {
    colors: {
			transparent: "transparent",
			black: "#22292f",
			"grey-darkest": "#3d4852",
			"grey-darker": "#606f7b",
			"grey-dark": "#8795a1",
			grey: "#b8c2cc",
			"grey-light": "#dae1e7",
			"grey-lighter": "#f1f5f8",
			"grey-lightest": "#f8fafc",
			white: "#ffffff"
    },
    fontFamily: {
      main: ["Open Sans", "sans-serif"],
			secondary: ["Lato", "sans-serif"]
    },
    extend: {
      textSizes: {
        xs: ".75rem", // 12px
        sm: ".875rem", // 14px
        base: "1rem", // 16px
        lg: "1.125rem", // 18px
        xl: "1.25rem", // 20px
        "2xl": "1.5rem", // 24px
        "3xl": "1.875rem", // 30px
        "4xl": "2.25rem", // 36px
        "5xl": "3rem", // 48px
        "6xl": "7rem" // 112px
      },
      borderRadius: {
        none: "0",
        sm: ".125rem",
        default: ".25rem",
        lg: ".5rem",
        full: "9999px"
      },
      maxWidth: {
        'xs': '4rem',
        'sm': '6rem',
        'md': '8rem',
        'lg': '12rem',
        'xl': '14rem',
        '2xl': '16rem',
        '3xl': '18rem',
        '4xl': '20rem',
        '5xl': '100rem',
        'full': '100%',
      },
    
    }
  },
  variants: {},
  plugins: []
}
