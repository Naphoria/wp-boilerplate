<?php $browserBackground = get_field('browser_update_background', 'option'); ?>
<div class="updateBrowser" style="background-image: url('<?php echo $browserBackground ['url']; ?>')">
    <div class="updateBrowser__wrapper">
        <div class="container">
            <div class="header">
                <div class="heading">
                    <h3>Please update your browser to one of the following</h3>
                    <p>This website is built and optimised for modern web browsers, for an optimum experience, please re-open in one of the browsers listed below:</p>
                </div>
                <div class="close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </div>
            </div>
            <ul>
                <li>
                    <a href="https://www.google.com/chrome/">Chrome</a>
                </li>
                <li>
                    <a href="https://www.apple.com/uk/safari/">Safari</a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-GB/firefox/new/">Fire Fox</a>
                </li>
                <li>
                    <a href="https://www.microsoft.com/en-us/edge">Edge</a>
                </li>
            </ul>
        </div>
    </div>
</div>