<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Starter_Theme
 */

?>

</div><!-- #content -->

<?php
$telephone = get_field('telephone', 'option');
$email = get_field('email', 'option');
$directionsApple = 'https://maps.apple.com/place?address=Calder%20Island%20Way%2C%20West%20Yorkshire%2C%20Wakefield%2C%20WF2%2C%20England&auid=18147592706717531932&ll=53.66336058202599%2C-1.5025252103805542&q=Northern%20Media';
$directionsGoogle = 'https://www.google.com/maps/dir//Northern+Media,+Unit+2,+Lakeside+Calder+Island+Way,+Wakefield+WF2+7AW/data=!4m7!4m6!1m1!4e2!1m2!1m1!1s0x48796702cbde30b5:0xcdcab2199a011c18!3e0?sa=X&ved=2ahUKEwj_-OXv8tDlAhVgQEEAHbgPClgQox0wAHoECAsQEQ';
?>

<div id="contact-mobile-sticky" class="contact-mobile-sticky">
    <div class="flex lg:flex-col">
        <a href="tel:<?php echo $telephone ?>" class="block w-1/3">
            <i class="fas fa-phone"></i>
        </a>
        <a href="mailto:<?php echo $email; ?>" class="block w-1/3">
            <i class="fas fa-envelope"></i>
        </a>
        <a href="<?php echo $directionsGoogle; ?>" class="block w-1/3">
            <i class="fas fa-map-marker-alt"></i>
        </a>
    </div>
</div>

<footer id="colophon" class="site-footer bg-grey-dark py-6 text-center lg:text-left">
    <div class="container">
        <nav class="menu">
            <?php
            if (has_nav_menu('footer-menu')) :
                wp_nav_menu(array(
                    'theme_location' => 'footer-menu', // Do not fall back to first non-empty menu.
                ));
            endif;
            ?>
        </nav>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script>
	/* Sample function that returns boolean in case the browser is Internet Explorer*/
	function isIE() {
		ua = navigator.userAgent;
		/* MSIE used to detect old browsers and Trident used to newer ones*/
		var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
		
		return is_ie; 
	}
	/* Create an alert to show if the browser is IE or not */
	if (isIE()){
		//alert('It is InternetExplorer');
		jQuery('.updateBrowser').addClass('updateBrowser--open');
	} else {
		//alert('It is NOT InternetExplorer');
		console.log('Good Browser');
	}
</script>

</body>

</html>