<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Starter_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site antialiased">

		<header id="masthead" class="site-header">
			<div class="container">
				<div class="flex items-center justify-between py-3">
					<div class="logo">
						<?php
						$logo = get_field('main_logo', 'option');
						?>
						<a href="<?php echo get_home_url() ?>">
							<img class="max-w-sm" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
						</a>
					</div>
					<nav class="menu">
						<div class="hidden lg:block">
							<?php
							if (has_nav_menu('primary-menu')) :
								wp_nav_menu(array(
									'theme_location' => 'primary-menu', // Do not fall back to first non-empty menu.
								));
							endif;
							?>
						</div>
						<div class="lg:hidden text-red">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</div>
					</nav>
				</div>
			</div>
		</header>

		<?php get_template_part( 'template-parts/partial', 'mobile-menu' ); ?>

		<?php get_template_part( 'template-parts/partial', 'browser' ); ?>

		<div id="content" class="site-content">
