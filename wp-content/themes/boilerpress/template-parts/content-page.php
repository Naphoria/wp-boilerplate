<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Starter Theme
 */

?>

<section id="page" class="page py-6">
	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<h1><?php the_title(); ?></h1>

			<div class="entry-image mb-8">
				<?php the_post_thumbnail(); ?>
			</div>

			<div class="entry-content">
				<?php the_content(); ?>
			</div>

		</article>
	</div>
</section>
