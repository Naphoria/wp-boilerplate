<?php
/**
 * Spike Starter Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package The_Starter_Theme
 */

if ( ! function_exists( 'spike_starter_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function spike_starter_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Spike Starter Theme, use a find and replace
		 * to change 'spike-starter' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'spike-starter', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary-menu' => esc_html__( 'Primary', 'spike-starter' ),
            'footer-menu' => esc_html__( 'Footer', 'spike-starter' ),
            'mobile-menu' => esc_html__( 'Mobile', 'spike-starter' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/**
		 * Add support for custom thumbnail sizes
        */

        /**
         * Add theme options page
        */

        if( function_exists('acf_add_options_page') ) {

            $option_page = acf_add_options_page(array(
                'page_title'    => 'Options',
                'menu_title'    => 'Options',
                'menu_slug'     => 'theme-general-settings',
                'capability'    => 'edit_posts',
                'redirect'  => false,
                // 'parent_slug' => 'options-general.php',
            ));

        }
	}
endif;
add_action( 'after_setup_theme', 'spike_starter_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function spike_starter_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'spike_starter_content_width', 640 );
}
add_action( 'after_setup_theme', 'spike_starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function spike_starter_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'spike-starter' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'spike-starter' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'spike_starter_widgets_init' );

/**
 * Prevent WP from loading jQuery by default.
 */
function remove_jquery_migrate( &$scripts){
    if(!is_admin()){
        $scripts->remove( 'jquery');
    }
}
add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );

/**
 * Enqueue scripts and styles.
 */
function spike_starter_scripts() {
	wp_enqueue_style( 'spike-starter-style', get_stylesheet_uri() );
	
    wp_enqueue_script( 'theme.js', get_stylesheet_directory_uri() . '/public/js/theme.js', array(), '', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'spike_starter_scripts' );


/**
 * Add Admin & Login Styles
 */
function admin_custom_style() {
	wp_enqueue_style('admin-styles', get_stylesheet_directory_uri() . '/admin.css');
  }
  add_action('admin_enqueue_scripts', 'admin_custom_style');
  add_action('login_enqueue_scripts', 'admin_custom_style');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Remove unnecessary scripts to speed up WP
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Prevent WP from adding extra margin when logged-in
 */
function remove_admin_margin() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('admin_bar_init', 'remove_admin_margin');

/**
 * Ensure all mail is routed through Northern Media's SMTP relay, via Mailjet
 */
function amend_sender_email( $original_email_address ) {
    return 'website@northernmediauk.com';
}
add_filter( 'wp_mail_from', 'amend_sender_email' );

