<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package The_Starter_Theme
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		


		<section class="error-404 not-found">
			<div class="container py-8">

				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e('Oops! That page can&rsquo;t be found.', '_s'); ?></h1>
				</header>

				<div class="page-content">
					<p><?php esc_html_e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', '_s'); ?></p>

					<form action="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>" method="GET" class="search-form flex">
						<input type="text" name="s" id="s" placeholder="Enter your search term" class="w-full bg-white px-4 py-2">
						<input type="submit" class="search-submit" value="Search" class="">
					</form>


				</div>
			</div>

		</section><!-- .error-404 -->

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
