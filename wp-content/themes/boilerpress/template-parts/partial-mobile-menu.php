<section id="mobileMenu" class="mobileMenu">
    <div class="container">
        <div class="flex items-center justify-between py-3">
            <div class="logo">
                <?php
                $logo = get_field('main_logo', 'option');
                ?>
                <img class="max-w-sm" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
            </div>
            <div class="exit text-red">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
        </div>
        <div class="menu py-8">
            <?php
            if (has_nav_menu('mobile-menu')) :
                wp_nav_menu(array(
                    'theme_location' => 'mobile-menu', // Do not fall back to first non-empty menu.
                ));
            endif;
            ?>
        </div>
        <div class="social border-t border-red border-solid">
            <?php
                $facebook = get_field('facebook_link', 'option');
                $twitter = get_field('twitter_link', 'option');
                $instagram = get_field('instagram_link', 'option');
                $linkedIn = get_field('linked_in_link', 'option');
                $youtube = get_field('youtube_link', 'option');
            ?>
            <ul class="list-reset flex py-4 justify-around">
                <?php if(isset($facebook)) : ?>
                    <li>
                        <a href="<?php echo $facebook; ?>">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(isset($twitter)) : ?>
                    <li>
                        <a href="<?php echo $twitter; ?>">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(isset($instagram)) : ?>
                    <li>
                        <a href="<?php echo $instagram; ?>">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(isset($linkedIn)) : ?>
                    <li>
                        <a href="<?php echo $linkedIn; ?>">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(isset($youtube)) : ?>
                    <li>
                        <a href="<?php echo $youtube; ?>">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>