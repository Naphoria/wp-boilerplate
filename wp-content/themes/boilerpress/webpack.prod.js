const path = require("path"); //Used to resolve path
const common = require("./webpack.common"); //Used to take influence from base file
const merge = require("webpack-merge"); //Used to merge common base file
const MiniCssExtractPlugin = require("mini-css-extract-plugin"); //Used to write the style.css file
const TailwindExtractor = require("./util/TailwindExtractor");

module.exports = merge(common, {
	mode: "production",
	output: {
		filename: "theme.js",
		path: path.resolve(__dirname, "public/js/")
	},
	plugins: [
		// Writes the styles to style.css
		new MiniCssExtractPlugin({
			filename: "../../style.css"
		})
	],
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					{ loader: MiniCssExtractPlugin.loader }, //Exports the CSS inside theme.js into style.css
					{ loader: "css-loader" }, // Used for the @import in the js file
					{
						loader: "postcss-loader", //Multiple css options to optimise the code
						options: {
							plugins: [
								require("precss"), //Lets you use Sass-like markup and staged CSS features in CSS
								require("tailwindcss"), //Adds Tailwind
								require("autoprefixer"), //Parse CSS and add vendor prefixes to CSS
								require("postcss-import")(), //Allows @ import
								require("cssnano"), //Minify
								// require("@fullhuman/postcss-purgecss")({
								// 	content: ["**/*.php"],
								// 	extractors: [
								// 		{
								// 		  extractor: TailwindExtractor,
								// 		  extensions: ['html', 'php']
								// 		}
								// 	],
								// 	rejected: true
								// }), //Removes unused css classes
								require("postcss-reporter")({ clearReportedMessages: true })

							]
						}
					},
					{ loader: "sass-loader" } //Compiles SASS to CSS
				]
			}
		]
	}
});
