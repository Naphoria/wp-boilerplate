//************* */
// Imports
//************* */

// Font Awesome
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

// jQuery - https://jquery.com/
import $ from "jquery";

// Swiper Slider - https://idangero.us/swiper/ (JS Image Slider)
import Swiper from "swiper";

// AOS - https://github.com/michalsnik/aos/ (Page load/ scroll down animations)
import AOS from "aos";

// Vanilla Lazy Load - https://github.com/verlok/lazyload/ (Loads images when in certain viewport/ before you see the image)
import LazyLoad from "vanilla-lazyload";

// Anime JS - https://animejs.com/documentation/ (Used to animate more specifically such as individual letters)
import anime from 'animejs';

// swup JS - https://swup.js.org/getting-started/ (Used for page transitions)
import Swup from 'swup';

// Async Web Font https://github.com/typekit/webfontloader
var WebFont = require("webfontloader");

// QuickLink Import used to prefetch internal links
import { listen, prefetch } from "quicklink";

// Relax js for paralax effects
import Rellax from 'rellax';

// Webpack SCSS import for Developmend Mode
import "../sass/main.scss";

//************************** */
// Page ready to fire code
//************************** */
$(document).ready(function() {
    // Parallax/ Relax.js
    var rellax = new Rellax('.rellax');

	// quicklink prefetch
	listen();

	// Enable Hover touch on mobile
	$('body').on('touchstart', function() {});

	// Initalise AOS
	AOS.init();

	// Instantiate LazyLoad()
	var myLazyLoad = new LazyLoad();

	// Basic Autoplay Swiper Slider with pagination and buttons
	var swiper = new Swiper(".swiper-container", {
		spaceBetween: 30,
		centeredSlides: true,
		autoplay: {
			delay: 2500,
			disableOnInteraction: false
		},
		pagination: {
			el: ".swiper-pagination",
			clickable: true
		},
		navigation: {
			nextEl: ".swiper-button-next",
			prevEl: ".swiper-button-prev"
		}
	});

	// load fonts asynchronously
	WebFont.load({
		// Change font: https://fonts.google.com/
		google: { families: ["Open+Sans:400,700"] }
	});

	// Fire Google event when 'tel:' or 'mailto:' links are clicked
	$("a[href^='tel:']").click(function() {
		gtag("event", "Contact via phone", {
			event_category: "Contact via phone ",
			event_label: "click",
			value: 1
		});
	});

	$("a[href^='mailto:']").click(function() {
		gtag("event", "Contact via email", {
			event_category: "Contact via email",
			event_label: "click",
			value: 2
		});
	});

	// Open mobile menu
	$('header .menu svg').click(function() {
		console.log('Open Menu');
		$('#mobileMenu').addClass('mobileMenu--open');
	});
	$('#mobileMenu .exit').click(function() {
		console.log('Open Menu');
		$('#mobileMenu').removeClass('mobileMenu--open');
	});

	
	// Animate Sticky Contact in
    if ( $(window).width() >= 992 ) {
		setTimeout(function(){
			$('#contact-mobile-sticky').addClass('contact-mobile-sticky--open');
		}, 3000);
	}

	// Close Browser Update
	$('.updateBrowser .close').click( function() {
		$('.updateBrowser .close').removeClass('updateBrowser--open');
	});

});
