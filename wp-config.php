<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3S2tE1azGND/648y5g7n4r5SiWf7FrUs6wx8PHhGcmF3A4KQ4HYrlPXj5OfiNDfUDddwllHDJbI4KZnljEBNmQ==');
define('SECURE_AUTH_KEY',  'WIgHmOgWUnWMIrJUKKmffUwJsm4odlnTVe+TjnlIuAe7T5DXBrRONZy8RIhxAGVXaZgQ6OljNgLf+gi9sq4+FA==');
define('LOGGED_IN_KEY',    'VSJgaU78o7aEvd6pzFPoVT/4jB/dOx5AVVO+54B54MSk9KjHFa0tChJrDFhfc8NYl9F1WA9IZ+T48BBUrS1ujw==');
define('NONCE_KEY',        'sxVYsAYsHGg6ZeEgv9vVznruqeR+AWxJxVu8sWQv5BFFc7wd7U4SDmQXaJ4R9znlNzJdkxjIgvKrEfjSEAgZug==');
define('AUTH_SALT',        '6cJfyPa4GoGZfSzbZ4EjoMEiC0kiqeyGM09OUHbjkUaYdEeowj79Ww0CVMcwKxE55sWVSrDubs9qOVlCrpz5Uw==');
define('SECURE_AUTH_SALT', 'hLczqIJMvItkSe4vGsBPdzCJmdlpu28rMGYhO7CuNOcDXjKeSGdT7gEAwkJj/ypvtEtrIqXHOKyWwl0RE+XiCw==');
define('LOGGED_IN_SALT',   '7tHvSstBjksjtlXobDmTK0lVXfjrPf62ezHcz4TcI0BcdntIH9M6/taWPD1DuWQjRrNO1sPYlPb7HECu4Cms9g==');
define('NONCE_SALT',       'fMAIrd2tIUyjQfkPRrXNnL4iOIIVy5CnSCwVhxxR9eeYW0lcbsCIUWENTeMWVAx1WMGfdaesxWLHUtIGYMZcuw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
