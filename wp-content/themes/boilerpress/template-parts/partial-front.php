<section id="site-intro" class="text-center py-8">
    <div class="container">
        <h1 class="font-bold">The Starter Theme</h1>
        <h2>A WordPress starter theme to increase development productivity</h2>
        <h3>Testing h3</h3>
        <h4>Testing h4</h4>
        <h5>Testing h5</h5>

        <h2>Starter Theme Features</h1>
        <ul class="mb-6">
            <li class="text-sm">-- Sass --</li>
            <li class="text-sm">-- Autoprefixer --</li>
            <li class="text-sm">-- CSS Minification --</li>
            <li class="text-sm">-- Webpack --</li>
            <li class="text-sm">-- Browsersync --</li>
            <li class="text-sm">-- ES6 --</li>
            <li class="text-sm">-- Tailwind CSS --</li>
            <li class="text-sm">-- Font Awesome --</li>
        </ul>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus ea, tempora maxime sapiente aut sequi doloremque laudantium, provident magnam iure impedit reiciendis dignissimos ab vero, iste inventore aperiam. Cumque, odio?</p>
        
        <a href="https://tailwindcss.com/" target="_blank" rel="nofollow" class="text-1xl underline">Let's get building!</a>
    </div>
</section>