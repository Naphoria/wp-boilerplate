# The Starter Theme
This starter theme is made to speed up WordPress theme development!

#### Features Include:
- Webpack with Browsersync
- CSS minification and removal of unused css
- Tailwind CSS
- Font Awesome 5